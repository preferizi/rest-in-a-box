# REST-in-a-box

## REST-in-a-box

REST-in-a-box is a comprehensive package for building fast and flexible REST APIs in PHP. It is based on the popular Laravel Lumen micro-framework and combines a range of Lumen packages into a reliable and harmonized solution that should work for a majority of REST API projects. Therefore REST-in-a-box will enforce certain development patterns, which we strongly believe to be the best way to build maintainable REST APIs.

## License

REST-in-a-box is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## Documentation

## Installation

### Server Requirements
The system requirements for REST-in-a-box are mainly defined by the Lumen framework its based on. Of course, all of these requirements are satisfied by the Vagrant configuration included within the package. So it's highly recommended that you use the provided Vagrant box as your local REST-in-a-box development environment.

However, if you choose to run a different setup, you will need to make sure your server meets the following requirements:

* PHP >= 5.5.9
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension

### Installing REST-in-a-box
REST-in-a-box utilizes Composer to manage its dependencies. So, before using it, make sure you have Composer installed on your machine.

Install it by issuing the Composer create-project command in your terminal:
```
composer create-project --prefer-dist rest-in-a-box/rest-in-a-box api
```

### Configuration

## Patterns
Repository Pattern
Dependency Injection
Transformers

## Resources
Lumen: Lumen documentation
Repositories: l5-repository documentation
Transformers: Fractal
Responses: Dingo???
