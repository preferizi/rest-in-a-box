<?php
namespace App;

use App\Console\Kernel;
use App\Exceptions\Handler;
use App\Providers\AppServiceProvider;
use App\Providers\EventServiceProvider;
use App\Providers\RepositoriesServiceProvider;
use Dingo\Api\Routing\Router;
use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Laravel\Lumen\Application;
use Illuminate\Contracts\Console\Kernel as KernelContract;
use RestInABox\Framework\Providers\RestApiServiceProvider;

require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv(__DIR__ . '/../'))->load();
} catch (InvalidPathException $e) {
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
*/
$app = new Application(
    realpath(__DIR__ . '/../')
);
$app->withFacades();
$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
*/
$app->singleton(ExceptionHandler::class, Handler::class);
$app->singleton(KernelContract::class, Kernel::class);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
*/
// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

/*
|--------------------------------------------------------------------------
| External Service Providers
|--------------------------------------------------------------------------
*/
$app->register(RestApiServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Register Application Service Providers
|--------------------------------------------------------------------------
*/
$app->register(AppServiceProvider::class);
$app->register(EventServiceProvider::class);
$app->register(RepositoriesServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Configure Application Settings
|--------------------------------------------------------------------------
*/
// config(['dot.path.to.setting' => 'value]);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
*/
$api = $app[Router::class];
$api->version('v1', [], function (Router $api) {
    require __DIR__ . '/../app/Http/routes.php';
});
return $app;
