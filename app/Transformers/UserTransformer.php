<?php
namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer
 * @package App\Transformers
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    /**
     * List of resources to available for include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];


    /**
     * Transform the model
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => (int)$user->id,
            'name' => $user->name,
            'email' => $user->email,
            'created_at' => ($user->created_at ? $user->created_at->format('c') : null),
            'updated_at' => ($user->updated_at ? $user->updated_at->format('c') : null)
        ];
    }
}
