<?php
namespace App\Http;

use App\Repositories\UserRepositoryInterface;
use App\Transformers\UserTransformer;
use Dingo\Api\Http\Response\Factory;
use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/** @var Router $api */
$api->get('/', function (Router $api) {
    return 'Hello World';
});
$api->get('users/{id}', function (UserRepositoryInterface $repository, Factory $response, $userId) {
    return $response->item($repository->find($userId), UserTransformer::class);
});
