<?php
namespace App\Repositories;

use App\Transformers\UserTransformer;
use App\User;
use League\Fractal\TransformerAbstract;
use RestInABox\Framework\Repository\Eloquent\BaseRepository;

/**
 * Class UserRepositoryEloquent
 * @package App\Repositories
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepositoryInterface
{
    /**
     * Specify Transformer class name
     *
     * @return TransformerAbstract
     */
    public function transformer()
    {
        return new UserTransformer();
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
}
