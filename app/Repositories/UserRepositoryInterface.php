<?php
namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryCriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use RestInABox\Framework\Repository\Contracts\TransformerAwareRepositoryInterface;
use RestInABox\Framework\Repository\Contracts\ValidationAwareRepositoryInterface;

/**
 * Interface UserRepositoryInterface
 * @package App\Repositories
 */
interface UserRepositoryInterface extends RepositoryInterface, RepositoryCriteriaInterface, ValidationAwareRepositoryInterface, TransformerAwareRepositoryInterface
{
}
