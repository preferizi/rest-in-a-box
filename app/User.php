<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Prettus\Repository\Contracts\Presentable;
use RestInABox\Framework\Repository\Constrains\PresentableTrait;

/**
 * Class User
 * @package App
 */
class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    Presentable
{
    use Authenticatable, Authorizable;
    use PresentableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
